# IBAs traslapadas:
La diferencia de la suma de las áreas de cada polígonos menos la suma de la disolución de polígonos da 627 ha 
```r
> st_area(iba_bl) %>% sum() - st_union(iba_bl) %>% st_area() %>% sum()
6273328 [m^2]
```
En el archivo ibas_traslapadas_birdlife.csv se detallan estos traslapes;
Los polígonos de Conabio no presentan traslape (en realidad es `0.01593018 [m^2]` para toda la capa, por lo que es ignorable)
Se evaluaron las concordancias geoespaciales con `st_equals_exact` ya que _había diferencias en los vértices de la mayoría de las IBAs_;

<img src="/plots_ibas/001_nombre_alfa.png"  width="300" height="300">

# Discrepancia de Nombres
Debido a inconsistencias en acentuación los dos archivos de polígonos de IBAs fueron empatados con una _unión difusa_
El archivo discrepancia_blife_conabio.csv contiene los nombres que no pudieron coincidir mediante unión difusa, así como las IBAs que no aparecen en uno u otro archivo.


# Rosetta Stone AOU Birdlife

Se unieron las checklist para aves de México, actualizadas de AOU y de Birdlife;

URLs de descarga utilizadas:

__AOU__
http://checklist.americanornithology.org/taxa.csv?type=charset%3Dutf-8%3Bsubspecies%3Dno%3B (actualiza en mismo link)

__Birdlife__
http://datazone.birdlife.org/userfiles/file/Species/Taxonomy/HBW-BirdLife_Checklist_v5_Dec20.zip 

Se hizo la `join` a través de los taxon grid de Avibase (usando web-scrapping)

Notas para facilitar la siguiente actualización:
1. Unir ambas tablas por una columna lowercase y sin hyphenation
2. Cambiar Birdlife a _Color_ y _Gray_ (escritura americana) para la unión

En la tabla hay una columna de problemáticas para la unión, la cual da detalle de las especies que son consideradas subespecies o aún no consideradas por Birdlife. _Esta parte fue hecha manualmente después de consolidar las columnas de Birdlife_.

## Diferencias detectadas:
Categorías de IUCN.
```r
  `consolidated_iucn != uicn`     n
  <lgl>                       <int>
1 FALSE                        1083
2 TRUE                           16
3 NA                             20
````

Nombres científicos.
```r
  `species != consolidated_sci_blife`     n
  <lgl>                               <int>
1 FALSE                                1050
2 TRUE                                   58
3 NA                                     11
```

Nombres comunes sin mayúsculas o hyphenation.
```
  `gsub("-", " ", tolower(nombre_ingles)) != gsub("-", " ", tolower(consolidated_common_blife))`     n
  <lgl>                                                                                          <int>
1 FALSE                                                                                            982
2 TRUE                                                                                             119
3 NA                                                                                                18
```

Escritura inglesa vs americana:
```r
birdos %>%
   filter(grepl("Gray|gray", nombre_ingles)) 
  `nombre_ingles != consolidated_common_blife`     n
  <lgl>                                        <int>
1 TRUE                                            26
```









